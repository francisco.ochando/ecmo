<img src= "http://2017.igem.org/wiki/images/c/c0/T--Aachen--Hardware_1_Pump_CAD.png">

**ECMO Project** 

Peristaltic Pump with Arduino


**Description**

http://biohackacademy.github.io/biofactory/class/8-pumps/

Desarrollo de una bomba peristáltica para el bombeo de líquido (desarrollo de la electrónica).

**References**

Proyecto ECMO de coronavirusmakers

https://gitlab.com/coronavirusmakers/ecmo

Proyecto Igem BioHack

http://2017.igem.org/Team:Aachen/Hardware

BioHack Academy Peristaltic pump

https://github.com/BioHackAcademy/BHA_PeristalticPump

**Requirements** 

http://biohackacademy.github.io/biofactory/class/8-pumps/requirements/

**Requisitos funcionales**

* Bombeo continuo
* Resolucion 1 ml
* Direccion Fwd/Rw
* Velocidad ajustable
* Indicador de estado

**Requisitos técnicos**

* Diametro del tubo (8mm??)
* Motor paso a paso
* Teclado dos botones
* Encoder rotativo
* Pantalla LCD I2C (16x2)



